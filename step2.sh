#!/bin/bash
aws cognito-idp list-user-pools --max-results 20

read -p "Enter user_pool_id: " USER_POOL_ID
read -p "Enter path for the file containing cognito_ids to delete: " COGNITO_IDS
echo "USER POOL ID : ($USER_POOL_ID) "
echo "COGNITO_IDS  : ($COGNITO_IDS)  "

if [ -f "$COGNITO_IDS" ]; then
	cat "$COGNITO_IDS" | while read line || [[ -n $line ]];
	do
	  if [[ "$line" != $'\n' ]] ; then
	   echo "Deleting user(cognito_id): $line " && aws cognito-idp admin-delete-user --user-pool-id $USER_POOL_ID --username $line
	  fi
	done
else
	echo "File $COGNITO_IDS does not exist."
fi

