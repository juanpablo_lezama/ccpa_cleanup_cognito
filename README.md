**Procedure to delete profiles in cognite using a list of profile_ids**


---

## Step 1

The first step is a bash script "step1.sh" that executes a query in the session_service database, it requires a list of profile_id's as input in the file: "query_profiles.sql", the script connects to the database (asking for host,database and user as inputs) and the result is exported to the temporary file "./cognito_ids.csv"

## Step 2
The second step is a bash script "step2.sh" that receives the user_pool_id (from cognito), and the file "./cognito_ids.csv" which is the output of the previous step, then iterates over the cognito_id's and execute the task *admin-delete-user* on cognito.