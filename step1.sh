#!/bin/bash
read -p "Enter PostgreSQL Host: " P_HOST
read -p "Enter PostgreSQL Database: " P_DATABASE
read -p "Enter PostgreSQL User: " P_USER
echo "HOST : ($P_HOST) "
echo "DATABASE : ($P_DATABASE) "
echo "USER: ($P_USER)"

psql -h "$P_HOST" -d "$P_DATABASE" -U $P_USER -c "\copy ($(<query_profiles.sql)) to ./cognito_ids.csv delimiter ',' csv;"

echo "File with cognito_ids generated on ./cognito_ids.csv"
